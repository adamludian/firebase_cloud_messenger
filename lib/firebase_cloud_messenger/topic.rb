module FirebaseCloudMessenger
  class Topic < Client

    def save(topic_id, tokens)
      retry_count = 0

      loop do
        response = make_fcm_request(topic_id, tokens)

        retry_count += 1
        if response_successful?(response) || retry_count > max_retry_count
          return message_from_response(response)
        elsif response.code == "401"
          refresh_access_token_info
        else
          raise Error.from_response(response)
        end
      end
    end

    def request_body(topic_id, tokens)
      {
          to: "/topics/#{topic_id}",
          registration_tokens: tokens
      }.to_json
    end

    def send_url
      URI 'https://iid.googleapis.com/iid/v1:batchAdd'
    end

    private

    def make_fcm_request(topic_id, tokens, conn = request_conn)
      conn.post(send_url.path,
                request_body(topic_id, tokens),
                request_headers)
    end
  end
end
